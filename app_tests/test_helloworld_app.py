# -*- coding: utf-8 -*-
import cherrypy
import unittest
import requests
from helloworld_springer_app import app
from mock import patch
from test import test_support
from base_cherry_py_test_case import BaseCherryPyTestCase

def setUpModule():
    env = test_support.EnvironmentVarGuard()
    env.set('HELLOWORLD_SPRINGER_APP_TIMEOUT', "1")
    env.set('HELLOWORLD_SPRINGER_APP_DATE_ENDPOINT', "http://test.date.service/")
    with env:    
        app.main()
setup_module = setUpModule

def tearDownModule():
    cherrypy.engine.exit()
teardown_module = tearDownModule

class TestCherryPyApp(BaseCherryPyTestCase):

    @patch('helloworld_springer_app.app.get_date')
    def test_get_date(self, patched_get_date):
        patched_get_date.return_value("2016-01-25 10:55:55 +0000")
        response = self.request('/')
        self.assertEqual(response.output_status, '200 OK')
        self.assertTrue('The current date is' in response.body[0])

    def test_get_not_existent_url(self):
        response = self.request('/not_existent')
        self.assertEqual(response.output_status, '404 Not Found')

    @patch('helloworld_springer_app.app.get_date')
    def test_get_timeout_if_timer_expires(self, patched_get_date):
        patched_get_date.side_effect = requests.exceptions.ReadTimeout("BOOM!")
        response = self.request('/')

    @patch('helloworld_springer_app.app.get_date')
    def test_post_reload_request(self, patched_get_date):
        response = self.request(path='/', method='POST', app_path='/reload', data="")
        self.assertEqual(response.output_status, '200 OK')

if __name__ == '__main__':
    unittest.main()
