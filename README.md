Platform Engineering Exercise
=============================
This repository is part of the Springer Nature candidate assessment for Systems Engineers and Platform Developers.

Trello board
------------

The instructions to use this repository are in the first card of
[this trello board](https://trello.com/b/5qMF0d5A/)

Technical details
=================

Setting the environment
-----------------------

Note: This assessment has been tested on Ubuntu 14.04. It should work on other platforms.

Base dependencies:

 * Python (Tested on 2.7.11, but should work on any python 2.7). We suggest installing virtualenv and pyenv. https://github.com/yyuu/ and https://github.com/yyuu/pyenv-virtualenv

Configuration

The application requires two configuration parameters:

`timeout`        the timeout in seconds for contacting the data service endpoint

`date_endpoint`  the url of the date_endpoint service

These two parameters can be either specified via the configuration file `/etc/helloworld_springer_app/config.yml` (yaml syntax):

```yaml
---
date_endpoint: "http://sleepy-thicket-8107.herokuapp.com/date"
timeout: 1
```

or defining the two following environment variables:

`HELLOWORLD_SPRINGER_APP_TIMEOUT`

`HELLOWORLD_SPRINGER_APP_DATE_ENDPOINT`

Tests
===================================
The test suite can be run using the command:

`python setup.py test`

Installation
===================================
The application can be built via the command

`python setup.py sdist`

under `$PROJECT/dist` you'll find a python package (tar.gz extension) installable via `pip install package_file`

Usage
=====================================
The application can be started in several ways:

`python $PROJECT/helloworld_springer_app/__main__.py`

or after installing it via pip

`helloworld_springer_app`

Two endpoints are currently provided:

|method|context|description|
|:----:|:-----:|:---------:|
|GET|/|loads the main application page|
|POST|/reload|reloads the configuration|
