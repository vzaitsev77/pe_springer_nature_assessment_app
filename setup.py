# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))


# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    # Application name:
    name="helloworld-springer-app",

    # Version number (initial):
    version="0.1.6",

    # Application author details:
    author="Claudio Benfatto",
    author_email="claudio.benfatto@springer.com",

    # Packages
    packages=["helloworld_springer_app"],

    # Include additional files into the package
    include_package_data=True,
     
    # Details
    url="",

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 2.7'
    ],

    # What does your project relate to?
    keywords='sample setuptools development',

    #
    # license="LICENSE.txt",
    description="HelloWorld Assessment application",

    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    install_requires=[
        "CherryPy == 4.0.0",
        "PyYAML == 3.11",
        "requests == 2.9.1",
        "Jinja2 == 2.8",
    ],

    test_suite = 'nose.collector',

    tests_require = [
        'mock',
        'nose',
    ],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        'console_scripts': [
            'helloworld_springer_app=helloworld_springer_app.__main__:main',
        ],
    }
)
