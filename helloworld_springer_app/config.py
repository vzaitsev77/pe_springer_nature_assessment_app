# -*- coding: utf-8 -*-

import os
import yaml

CONFIG_FILE="/etc/helloworld_springer_app/config.yml"

#configuration is initially empty
configuration = {}
env_variables = ['date_endpoint', 'timeout']

def _parse_config_file():
	config = {}
	if os.path.isfile(CONFIG_FILE):
	    try:
	        with open(CONFIG_FILE) as config_file:
	            yaml_conf = yaml.safe_load(config_file)
                if yaml_conf:
                    config = yaml_conf
	    except IOError:
	        print("Error opening configuration file")
	return config

def _parse_config_env():
    config = {}
    for variable in env_variables:
        value = os.environ.get("HELLOWORLD_SPRINGER_APP_" + variable.upper(), None)
        if value:
            config[variable] = value
    return config

def _check_variables(temp_conf):
    success = True
    for variable in env_variables:
        if not variable in temp_conf or not temp_conf[variable]:
            success = False
            print ("#############\n" +
                  "variable %s must be defined either in " % variable +
                  "%s or as the environment variable " % CONFIG_FILE +
                  "HELLOWORLD_SPRINGER_APP_%s\n" % variable.upper() +
                  "###################")
    return success

def load_config():
    """
    Loads the config giving priority to the environment variables
    """
    file_config = _parse_config_file()
    env_config  = _parse_config_env()

    temp_conf = {}
    temp_conf.update(file_config)
    temp_conf.update(env_config)

    if _check_variables(temp_conf):
        configuration.update(temp_conf)

def reload_config():
    load_config()
    print configuration
